<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/firm', 'FirmMgmtController@index')->name('firm');
Route::name('/firm-create',[
	'firm'=>'FirmMgmtController@create',
	'as'=>'firm.create']);

Route::get('/firm-create','FirmMgmtController@create')->name("firm-create");
Route::post('/firm-store','FirmMgmtController@store')->name("firm-store");
Route::get('/firm-show/{id?}','FirmMgmtController@show')->name("firm-show");
Route::get('/firm-edit/{id?}','FirmMgmtController@edit')->name("firm-edit");
Route::get('/firm-status/{id?}/{status?}','FirmMgmtController@changeStatus')->name("firm-status");
Route::patch('/firm-update/{id?}','FirmMgmtController@update')->name("firm-update");
Route::delete('/firm-destroy/{id?}','FirmMgmtController@destroy')->name("firm-destroy");
/*Route::name('web.firm.show')->any('/firm/show','FirmMgmtController@show');
Route::name('web.firm.edit')->any('/firm/edit/{id?}','FirmMgmtController@edit');
Route::name('web.firm.destroy')->any('/firm/destroy/{id?}','FirmMgmtController@destroy');
*/
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('api','APIController');
//Route::get('/dependent-dropdown','APIController@index')->name("dependent-dropdown");
Route::get('/api/checkaccess/{id?}/{fuid?}','APIController@checkaccess')->name("checkaccess");
Route::post('/api/get-state-list','APIController@getStateList')->name("get-state-list");
Route::post('/api/get-city-list','APIController@getCityList')->name("get-state-city");
Route::get('get-curl', 'HomeController@getCURL')->name('get-curl');
Route::get('/user-cases/{user?}/{id?}', 'FirmMgmtController@usercases')->name('user-cases');
Route::get('/feature-request', 'FirmMgmtController@feature_request')->name('feature-request');
Route::get('/view-feature-request/{id?}', 'FirmMgmtController@view_feature_requst')->name('view-feature-request');

Route::post('/store-bill','BillingController@store')->name("store-bill");
Route::get('/billing-list', 'BillingController@index')->name('billing-list');
Route::get('/billing-report', 'BillingController@billing_report')->name('billing-report');
Route::post('/billing-report-ajax', 'BillingController@billing_report')->name('billing-report-ajax');
Route::get('/outstanding-bills', 'BillingController@outstanding_bill')->name('outstanding-bills');
Route::get('/show-bill/{id?}', 'BillingController@show')->name('show-bill');
Route::post('/resendinvoice', 'BillingController@resendinvoice')->name('resendinvoice');
Route::post('/invoicepdf', 'BillingController@invoicepdf')->name('invoicepdf');
Route::get('/client-report', 'BillingController@clientreport')->name('client-report');
Route::get('/case-report', 'BillingController@casereportclientwise')->name('case-report');
Route::post('/case-report-ajax', 'BillingController@casereportclientwise')->name('case-report-ajax');
Route::get('/userlog-list', 'FirmMgmtController@listfirmuserlog')->name('userlog-list');
Route::get('/user-log/{id?}', 'FirmMgmtController@getuserlogs')->name('user-log');
Route::get('/setupchecklist', 'FirmMgmtController@getsetupchecklist')->name('setupchecklist');
Route::post('/view-setupchecklist', 'FirmMgmtController@viewsetupchecklist')->name('view-setupchecklist');
Route::get('/bug-report-list', 'FirmMgmtController@bugreportlist')->name('bug-report-list');
Route::get('/firm-bug-report/{id?}', 'FirmMgmtController@firmviewbugreport')->name('firm-bug-report');
Route::get('/view-bug-report/{id?}/{firm?}', 'FirmMgmtController@viewbugreport')->name('view-bug-report');
