<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class invoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       //echo "<pre>"; print_r($this->data); exit;
        return $this->from($this->data['from'])
        ->subject('OutStanding invoice')
        ->view('email.invoice')
        ->with([
            'resendinvoice' => $this->data['resendinvoice'][0],
            'dev_modules' => $this->data['dev_modules'],
        ]);
    }
}
