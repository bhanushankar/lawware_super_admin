<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing_mgmt extends Model
{
   	protected $table = 'billing_mgmts';
    protected $fillable = [
		'firm_id',
		'module_ids',	
		'total_amount',
		'created_at',
		'start_date',	
		'updated_at',
		'end_date',
		'bill_status'
    ];
}
