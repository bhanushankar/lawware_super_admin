<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dev_modules extends Model
{
 	protected $table = 'dev_modules';
    protected $fillable = [
    	'module_name',
		'module_price',
		'created_at'
    ];
    
}
