<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lawfirm_mgmt;
use App\Billing_mgmt;
use App\Dev_modules;
use DB;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Mail;
use App\Mail\invoice;
use PDF;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $firm_bill_list = DB::table("lawfirm_mgmt")->select("firm_name","id")->get();
        $dev_module_list = DB::table("dev_modules")->select("module_name","module_price","id")->get();
         $billing_list = DB::table('billing_mgmts')->select("billing_mgmts.*","lawfirm_mgmt.firm_name as firm_name")
            ->leftJoin('lawfirm_mgmt', 'lawfirm_mgmt.id', '=', 'billing_mgmts.firm_id')->orderByRaw('billing_mgmts.created_at DESC')
            ->get();
        $available_months = DB::table('billing_mgmts')
        ->select(DB::raw('MONTHNAME(billing_mgmts.created_at) as month' ))
        ->groupBy(DB::raw('month'))
        ->get();
        return view('billing.billing_list',compact('firm_bill_list','dev_module_list','billing_list','available_months'))->with('i');

        
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $st_dt= date('Y-m-d H:i:s');
        $end_dt=  date('Y-m-d H:i:s',strtotime("+1 month",strtotime(date('Y-m-d H:i:s'))));
        $firm_id = $_POST['module_fimr_name'];
        $data = array(
            'firm_id'=>$firm_id,
            'total_amount'=> $_POST['total_amount'],
            'module_ids'=> $_POST['module_ids'],
            'created_at'=> $st_dt,
            'start_date'=> $st_dt,
            'end_date'=> $end_dt,
            'bill_status'=>$_POST['bill_status']
        );
        $check_monthly = DB::table('billing_mgmts')->select('*')->where('end_date', '>=',$st_dt)->where('end_date','<=',$end_dt)->where('firm_id','=',$firm_id)->get();
        if(!empty($check_monthly)){
            die(json_encode($data = array('success'=>'0','msg'=>'Already Subscribed for current month')));
        }
        $id = Billing_mgmt::create($data)->id;
        if($id)
            { die(json_encode($data = array('success'=>'1','msg'=>'Firm created successfully')));}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=0)
    {
        $bill_data = DB::table('billing_mgmts')->select('*')->where('id','=',$id)->first();
        die(json_encode($bill_data));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function billing_report()
    {
        $years_start = date('Y',strtotime("01-01-2000"));
        $current_year =date("Y");
        $total_count='';
        $resquest_from = isset($_REQUEST['ajax_req']) ? $_REQUEST['ajax_req'] :'';
        $firm_id = isset($_REQUEST['firm_id']) ? $_REQUEST['firm_id'] :'1';
        $year = isset($_REQUEST['year']) ? $_REQUEST['year'] : $current_year;
        $bill_amount_year = DB::table('billing_mgmts')
        ->select(DB::raw('sum(total_amount) as total, CONCAT(MONTHNAME(billing_mgmts.created_at),"-",YEAR(billing_mgmts.created_at)) as month' ))
        ->where(DB::raw('YEAR(created_at)'), '=', DB::raw($year))
        ->groupBy(DB::raw('month'))
        ->get();
        $data=array();
        $i=0;
        foreach ($bill_amount_year as $value) {
            $data[$i]['category'] = date('M y',strtotime($value->month));
            $data[$i]['column-1'] = $value->total;
            $i++;
        }
        $total_count = !empty($data) ? count($data) : 0 ;
        $bill_data_yearly = json_encode($data);
        $firmlist_selectbox = DB::table('lawfirm_mgmt')->select(DB::raw('lawfirm_mgmt.id,lawfirm_mgmt.firm_name'))->get();
        $bill_undert_this_month = DB::table('lawfirm_mgmt')
        ->select(DB::raw('lawfirm_mgmt.*,billing_mgmts.*,sum(billing_mgmts.total_amount) as total,CONCAT(MONTHNAME(billing_mgmts.created_at),"-",YEAR(billing_mgmts.created_at)) as month'))
        ->leftjoin('billing_mgmts','lawfirm_mgmt.id','=','billing_mgmts.firm_id')
        ->groupBy(DB::raw('month'))->orderByRaw('lawfirm_mgmt.id desc')->get();
        
        if ($resquest_from ==''){
            return view('billing.billing_report',compact('bill_data_yearly','bill_undert_this_month','years_start','current_year','firmlist_selectbox'))->with('i');
        }
        else{
            die(json_encode(array('bill_data_yearly'=>$bill_data_yearly,'bill_undert_this_month'=>$bill_undert_this_month,'years_start'=>$years_start,'current_year'=>$current_year,'firmlist_selectbox'=>$firmlist_selectbox,'total_count'=>$total_count))); 
        }
    }
    public function outstanding_bill()
    {
        //$firm_bill_list = DB::table("lawfirm_mgmt")->select("firm_name","id")->get();
        $outstanding_bill = DB::table('lawfirm_mgmt')
        ->select(DB::raw('lawfirm_mgmt.*,billing_mgmts.*,sum(billing_mgmts.total_amount) as total,CONCAT(MONTHNAME(billing_mgmts.created_at),"-",YEAR(billing_mgmts.created_at)) as month,billing_mgmts.id as bill_id'))
        ->leftjoin('billing_mgmts','lawfirm_mgmt.id','=','billing_mgmts.firm_id')
        ->where('billing_mgmts.bill_status','=','1')
        ->groupBy(DB::raw('month'))->orderByRaw('lawfirm_mgmt.id desc')->get();
        //echo "<pre>"; print_r($outstanding_bill); exit;
        return view('billing.outstanding_bill',compact('outstanding_bill'))->with('i');
    }
    public function resendinvoice()
    {
        $firm_id = $_REQUEST['firm_id'];
        $bill_id = $_REQUEST['bill_id'];
        $resendinvoice = DB::table('lawfirm_mgmt')
        ->select(DB::raw('lawfirm_mgmt.*,billing_mgmts.*,sum(billing_mgmts.total_amount) as total,CONCAT(MONTHNAME(billing_mgmts.created_at),"-",YEAR(billing_mgmts.created_at)) as month'))
        ->leftjoin('billing_mgmts','lawfirm_mgmt.id','=','billing_mgmts.firm_id')
        ->where('billing_mgmts.bill_status','=','1')
        ->where('lawfirm_mgmt.id','=',$firm_id)
        ->where('billing_mgmts.id','=',$bill_id)
        ->groupBy(DB::raw('month'))->orderByRaw('lawfirm_mgmt.id desc')->get();
        $dev_modules = DB::table('dev_modules')->select('dev_modules.*')->get();
        //echo "<pre>"; print_r($resendinvoice); exit;
       Mail::to('bhavin.joshi84@gmail.com')
        ->send(new invoice(array('resendinvoice'=>$resendinvoice,'dev_modules'=>$dev_modules,'from'=>'notification@iodlawware.com')));
       
        //$data = view('email.invoice',compact('dev_modules','resendinvoice'))->with('i');
        exit;
    }
    public function invoicepdf()
    {
        $firm_id = $_REQUEST['firm_id'];
        $bill_id = $_REQUEST['bill_id'];
        $resendinvoice = DB::table('lawfirm_mgmt')
        ->select(DB::raw('lawfirm_mgmt.*,billing_mgmts.*,sum(billing_mgmts.total_amount) as total,CONCAT(MONTHNAME(billing_mgmts.created_at),"-",YEAR(billing_mgmts.created_at)) as month,country.name as country,state.name as state,city.name as city'))
        ->leftjoin('billing_mgmts','lawfirm_mgmt.id','=','billing_mgmts.firm_id')
        ->leftJoin('country', 'lawfirm_mgmt.firm_country', '=', 'country.id')
        ->leftJoin('state', 'lawfirm_mgmt.firm_state', '=', 'state.id')
        ->leftJoin('city', 'lawfirm_mgmt.firm_city', '=', 'city.id')
        ->where('lawfirm_mgmt.id','=',$firm_id)
        ->where('billing_mgmts.id','=',$bill_id)
        ->groupBy(DB::raw('month'))->orderByRaw('lawfirm_mgmt.id desc')->get();
        $resendinvoice = $resendinvoice[0];
        $dev_modules = DB::table('dev_modules')->select('dev_modules.*')->get();
        PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,'defaultFont' => 'sans-serif','defaultPaperSize'=>'a4','isPhpEnabled'=>true]);
        $html = view('email.invoice',compact('dev_modules','resendinvoice'));
        $pdf = PDF::loadHTML($html)->stream();
        $url = public_path().'/';
        $filename = time().'invoice.pdf';
        $myfile = file_put_contents($url.$filename, $pdf);
        return $filename;
    }
    public function clientreport() /* number of client in year month wise*/
    {
        $client_report_year = DB::table('lawfirm_mgmt')
        ->select(DB::raw('count(id) as clients,MONTHNAME(created_at) as month , CONCAT(MONTHNAME(created_at),"-",YEAR(created_at)) as monthyear'))
        ->groupBy('monthyear')->orderByRaw('created_at ASC')->get();
        $data=array();
        $i=0;
        foreach ($client_report_year as $value) {
            $data[$i]['category'] = date('M y',strtotime($value->monthyear));
            $data[$i]['column-1'] = $value->clients;
            $i++;
        }
        $client_report_year = json_encode($data);
        return view('other.client_report',compact('client_report_year')); 
    }
    public function casereportclientwise()
    {
        $years_start = date('Y',strtotime("01-01-2000"));
        $current_year =date("Y");
        $resquest_from = isset($_REQUEST['ajax_req']) ? $_REQUEST['ajax_req'] :'';
        $firm_id = isset($_REQUEST['firm_id']) ? $_REQUEST['firm_id'] :'1';
        $year = isset($_REQUEST['year']) ? $_REQUEST['year'] : $current_year;
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
        ->where('lawfirm_mgmt.id', '=',$firm_id)->first();
        $firmlist_selectbox = DB::table('lawfirm_mgmt')->select(DB::raw('lawfirm_mgmt.id,lawfirm_mgmt.firm_name'))->get();
        $url_firm_details = $firmlist->firm_subdomain.'/api/getcasereport';
        //$url_firm_details = 'http://localhost/lawware/api/getcasereport';
        $respose = Curl::to($url_firm_details)->withData(array('year'=>$year))->post();
        $domain_response = !empty($respose) ? json_decode($respose) : array();
        //echo "<pre>"; print_r($domain_response); exit;
        $data =array();
        $pie_data=array();
        if(!empty($domain_response)){
            $total_count = $domain_response->total_count;
            $i=0;
            foreach ($domain_response->case_report_data as $value) {
                $data[$i]['category'] = date('M y',strtotime($value->monthyear));
                $data[$i]['column-1'] = $value->nocase;
                $i++;
            }
            $case_report_nocase = json_encode($data);
            $i=0;
            foreach ($domain_response->case_report_data as $value) {
                $pie_data[$i]['category'] = date('M y',strtotime($value->monthyear));
                $pie_data[$i]['column-1'] = round(($value->nocase * 100 )/ $total_count,2);
                $i++;
            }
            $case_report_pie = json_encode($pie_data);
        }
        else
        { 
            $case_report_nocase =json_encode(array());
            $case_report_pie =json_encode(array());
            $total_count =0;
        }
        if ($resquest_from ==''){
            return view('other.case_report',compact('case_report_nocase','years_start','current_year','case_report_pie','total_count','firmlist_selectbox'))->with('i');
        }
        else{
            die(json_encode(array('case_report_nocase'=>$case_report_nocase,'case_report_pie'=>$case_report_pie,'total_count'=>$total_count,'firmlist_selectbox'=>$firmlist_selectbox)));   
        }
    }

}
