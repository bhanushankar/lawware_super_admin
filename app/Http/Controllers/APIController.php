<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
class APIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        //
    }
    public function getStateList(Request $request)
    {
        $states = DB::table("state")
                    ->where("country_id",$request->country_id)
                    ->select("name","id")->get();
        return response()->json($states);
    }
    public function getCityList(Request $request)
    {
        $cities = DB::table("city")
                    ->where("state_id",$request->state_id)
                    ->select("name","id")->get();
                    //->lists("name","id");
        return response()->json($cities);
    }

    public function checkaccess(Request $request)
    {
        //echo 'in checkaccess'; print_r($request); exit;
        //DB::enableQueryLog();
        $data = DB::table('lawfirm_mgmt')
                ->where('firm_unique_id',$request->fuid)
                ->where('id',$request->id)
                ->where('firm_status','<>','4')
                    ->select("*")->get();
        //dd(DB::getQueryLog()); exit;
        if(!empty($data)){
            return response()->json($data);
        }
        else{
            return response()->json('0');
        }
    }

}
