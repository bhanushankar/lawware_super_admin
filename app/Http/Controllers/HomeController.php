<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Lawfirm_mgmt;
use DB;
use Ixudra\Curl\Facades\Curl;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $firmcount = Lawfirm_mgmt::count();
        $firm_no_of_users = Lawfirm_mgmt::sum('firm_no_of_users');
        //$firm_no_of_cases = Lawfirm_mgmt::sum('firm_no_of_cases');
        $firm_no_of_inquiry = Lawfirm_mgmt::where(['firm_status'=>1])->get()->count();
        return view('home', compact('firmcount','firm_no_of_users','firm_no_of_inquiry'));
    }
}
