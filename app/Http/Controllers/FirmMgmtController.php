<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lawfirm_mgmt;
use DB;
use Ixudra\Curl\Facades\Curl;
class FirmMgmtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $firmlist = Lawfirm_mgmt::latest()->paginate(5);
        return view('firm.index',compact('firmlist'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = DB::table("country")->select("name","id")->get();
        return view('firm.create',compact('countries'));
        //return view('firmlist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'firm_name'=> 'required',
            'firm_address'=> 'required',
            'firm_state' => 'required',
            'firm_country'=> 'required',
            'firm_city'=> 'required',
            'firm_contact_number'=> 'required',
            //'firm_fax',
            'firm_email'=> 'required|email',
            'firm_concern_person'=> 'required',
            'firm_ip'=> 'required',
            'firm_subdomain'=> 'required',
            //'firm_logo',
           // 'firm_support_person',
            //'firm_support_contact',
            'firm_support_email'=>'email',
            //'created_at',
            //'updated_at',
        ],
        [ 'firm_subdomain.required' => 'The Domain name of firm field is required.'
        ]);
       /* $imageName = time().'.'.request()->firm_logo->getClientOriginalExtension();
        request()->firm_logo->move(public_path('firm_logo'), $imageName);
        request()->firm_logo = $imageName;*/
        $id = Lawfirm_mgmt::create($request->all())->id;
        if($request->hasfile('firm_logo')){
            $file = $request->file('firm_logo');
            $imageName = time().'.'.strtolower($file->getClientOriginalExtension());
            $request->firm_logo->move(public_path('firm_logo'), $imageName);

            Lawfirm_mgmt::find($id)->update(['firm_logo'=>$imageName]);
        }

        return redirect()->route('firm')
                        ->with('success','Firm created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     curl info
     http://itsolutionstuff.com/post/laravel-5-curl-request-example-using-ixudra-curl-packageexample.html
     */
    public function show($id)
    {
        //$firmlist =Lawfirm_mgmt::find($id);
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*","country.name as country","state.name as state","city.name as city")
            ->leftJoin('country', 'lawfirm_mgmt.firm_country', '=', 'country.id')
            ->leftJoin('state', 'lawfirm_mgmt.firm_state', '=', 'state.id')
            ->leftJoin('city', 'lawfirm_mgmt.firm_city', '=', 'city.id')
            ->where('lawfirm_mgmt.id', '=',$id)
            ->get();
        $firmlist = $firmlist[0];
        $url_firm_details = $firmlist->firm_subdomain.'/api/getfirmdetails';
        /*$url_user_list = $firmlist->firm_subdomain.'/api/getusers';
        $url_case_count = $firmlist->firm_subdomain.'/api/getcasecount';
        $respose = Curl::to($url_user_list)->get();
        $userlist = !empty($respose) ? json_decode($respose) : array();
        if(!empty($userlist) && $userlist->status == 1)
        {
            $userlist = $userlist->staff;
        }
        else{
            $userlist = array();
        }
        $casecount = Curl::to($url_case_count)->get();
        $casecount = (!empty($casecount)) ? json_decode($casecount) : array();*/
        $respose = Curl::to($url_firm_details)->get();
        $domain_response = !empty($respose) ? json_decode($respose) : array();
        $userlist = array();
        $casecount =array();
        $admin_data =array();
        if(!empty($domain_response)){
            $userlist = $domain_response->staff;
            $casecount = $domain_response->casecount;
            $admin_data = $domain_response->admin_data;
            $feature_request = $domain_response->feature_request;

            if(empty($firmlist->firm_admin_username) && empty($firmlist->firm_admin_password))
            {
                Lawfirm_mgmt::where('id',$firmlist->id)->update(['firm_admin_username'=>$admin_data->sa_user_name,
                    'firm_admin_password'=>$admin_data->sa_password]);
            }
            if(count($userlist) > $firmlist->firm_no_of_users){
                 Lawfirm_mgmt::where('id',$firmlist->id)->update(['firm_no_of_users'=>count($userlist)]);
            }
            if($casecount > $firmlist->firm_no_of_cases){
                 Lawfirm_mgmt::where('id',$firmlist->id)->update(['firm_no_of_cases'=>$casecount]);
            }
            if($feature_request > $firmlist->firm_feature_request){
                 Lawfirm_mgmt::where('id',$firmlist->id)->update(['firm_feature_request'=>$feature_request]);
            }

            if(empty($firmlist->firm_unique_id)){
                $firm_unique_id_gen = md5(bin2hex(openssl_random_pseudo_bytes(10)).strval(time()));
                 Lawfirm_mgmt::where('id',$firmlist->id)->update(['firm_unique_id'=>$firm_unique_id_gen]);
                 $admin_firm_details_update = $firmlist->firm_subdomain.'/api/updatedata';
                 $respose = Curl::to($admin_firm_details_update)
                    ->withData(array('table'=>'support_checklist','fields'=>array('uniqueid'=>$firm_unique_id_gen,'firm_id'=>$firmlist->id),'condition'=>array('id'=>'1')))->post();
            }
        }


        return view('firm.show',compact('firmlist','userlist','casecount'))->with('i');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $firmlist = Lawfirm_mgmt::find($id);
        $country_id = $firmlist->firm_country;
        $state_id = $firmlist->firm_state;
        $city_id = $firmlist->firm_city;
        $countries = DB::table("country")->select("name","id")->get();
        $state = DB::table("state")->select("name","id")->get();
        $city = DB::table("city")->select("name","id")->get();
        return view('firm.edit',compact('firmlist','countries','country_id','state_id','city_id','state','city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
           'firm_name'=> 'required',
            'firm_address'=> 'required',
            'firm_state' => 'required',
            'firm_country'=> 'required',
            'firm_city'=> 'required',
            'firm_contact_number'=> 'required',
            //'firm_fax',
            'firm_email'=> 'required',
            'firm_concern_person'=> 'required',
            'firm_ip'=> 'required',
            'firm_subdomain'=> 'required',
            //'firm_logo'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'firm_support_person',
            //'firm_support_contact',
            //'firm_support_email',
            //'created_at',
            //'updated_at',
        ]);
        Lawfirm_mgmt::find($id)->update($request->all());
        if($request->hasfile('firm_logo')){
            $file = $request->file('firm_logo');
            $imageName = time().'.'.strtolower($file->getClientOriginalExtension());
            $request->firm_logo->move(public_path('firm_logo'), $imageName);
            //$filename = $file->getClientOriginalName();
            Lawfirm_mgmt::find($id)->update(['firm_logo'=>$imageName]);
        }
        return redirect()->route('firm')
                        ->with('success','Firm updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lawfirm_mgmt::find($id)->delete();
        return redirect()->route('firm')
                        ->with('success','Firm deleted successfully');
    }
    public function usercases($user,$id)
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*","country.name as country","state.name as state","city.name as city")
            ->leftJoin('country', 'lawfirm_mgmt.firm_country', '=', 'country.id')
            ->leftJoin('state', 'lawfirm_mgmt.firm_state', '=', 'state.id')
            ->leftJoin('city', 'lawfirm_mgmt.firm_city', '=', 'city.id')
            ->where('lawfirm_mgmt.id', '=',$id)
            ->get();
        $firmlist = $firmlist[0];
        $url_case_user = $firmlist->firm_subdomain.'/api/getcasesofuser';
        $respose = Curl::to($url_case_user)->withData(['initials'=>$user])
                ->post();
        $caselist = !empty($respose) ? json_decode($respose) : array();
        if(!empty($caselist) && $caselist->status == 1)
        {
            $caselist = $caselist->allcases;
        }
        else{
            $caselist = array();
        }
        return view('firm.caselist',compact('caselist','id'))->with('i');

    }
    public function feature_request()
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->get();
        return view('other.feature_request',compact('firmlist'))->with('i');

    }
    public function view_feature_requst($id){
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->where('lawfirm_mgmt.id', '=',$id)
            ->get();
        $firmlist = $firmlist[0];
        $url_feature_requst = $firmlist->firm_subdomain.'/api/getfeaturerequest';
        $respose = Curl::to($url_feature_requst)->get();
        $feature_request_list = !empty($respose) ? json_decode($respose) : array();
        return view('other.view_feature_request',compact('feature_request_list','firmlist'))->with('i');
    }
    public function listfirmuserlog()
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")->get();
        return view('firm.userloglist',compact('firmlist'))->with('i');
    }
    public function getuserlogs($id)
    {
         $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->where('lawfirm_mgmt.id', '=',$id)
            ->get();
        $firmlist = $firmlist[0];
        $url_requst = $firmlist->firm_subdomain.'/api/getuserlog';
        //$url_requst = 'http://localhost/lawware/api/getuserlog';
        $respose = Curl::to($url_requst)->get();
        $request_list = !empty($respose) ? json_decode($respose) : array();
        return view('firm.userlog',compact('request_list','firmlist'))->with('i');
    }
    public function getsetupchecklist()
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")->get();
        return view('firm.setupchecklist',compact('firmlist'))->with('i');
    }
    public function viewsetupchecklist(Request $request)
    {
        $id = $request->firm_id;
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->where('lawfirm_mgmt.id', '=',$id)->first();
        $url_requst = $firmlist->firm_subdomain.'/api/getchecklist';
        if(!@file_get_contents($url_requst)){
            die(json_encode(array('status'=>0)));
        }
        $respose = Curl::to($url_requst)->get();
        $request_list = !empty($respose) ? json_decode($respose) : array();
        $data = '';
        $divideby=7;
        $count =0;
        $percent=0;
        if(!empty($request_list->checklist) && $request_list->status !=0)
        {
            $slc = $request_list->checklist[0];
            foreach ($slc as $key => $value) {
                if(($value !=0 && $value !='') || $value==1){ $count++;}
            }
            $percent =  round(($count * 100 ) /$divideby,2);
            $data .="<table class='table'>";
            $data .="<tr>";
            $data .="<td class='text-info'>Local Server :</td>";
            $data .="<td>";
            $data .= ($slc->install_xampp ==1) ? "<span class='text-success'>Done</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>Code Integration :</td>";
            $data .="<td>";
            $data .=($slc->code ==1) ? "<span class='text-success'>Done</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>Data Migrations :</td>";
            $data .="<td>";
            $data .= ($slc->database_migration ==1) ? "<span class='text-success'>Done</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>Chat Server :</td>";
            $data .="<td>";
            $data .= ($slc->chat_server ==1) ? "<span class='text-success'>Done</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>User Created :</td>";
            $data .="<td>";
            $data .=($slc->created_users ==1) ? "<span class='text-success'>Done</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>Public Domain :</td>";
            $data .="<td>";
            $data .= ($slc->public_domain !='') ? "<span class='text-success'>$slc->public_domain</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>IP Address :</td>";
            $data .="<td>";
            $data .= ($slc->ip_address !='') ? "<span class='text-success'>$slc->ip_address</span>" : "<span class='text-danger'>Pending</span>";
            $data .="</td>";
            $data .="</tr>";

            $data .="<tr>";
            $data .="<td class='text-info'>Last Updated on:</td>";
            $data .="<td>";
            $data .=date('m-d-Y H:i',strtotime($slc->updated_date));
            $data .="</td>";
            $data .="</tr>";

            $data .= "</table>";
            die(json_encode(array('data'=>$data,'status'=>1,'firm_name'=>$firmlist->firm_name,'percent'=>$percent)));
        }
        else{
            die(json_encode(array('status'=>0)));
        }

    }
    public function bugreportlist()
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")->get();
        return view('other.bug_report_list',compact('firmlist'))->with('i');
    }
    public function firmviewbugreport($id)
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->where('lawfirm_mgmt.id', '=',$id)->first();
        $url_requst = $firmlist->firm_subdomain.'/api/getbugreport';
        //$url_requst = 'http://localhost/lawware/api/getbugreport';
        /*if(!@file_get_contents($url_requst)){
            die(json_encode(array('status'=>0)));
        }*/
        $respose = Curl::to($url_requst)->get();
        //$respose = Curl::to($url_requst)->withData(['id'=>$id])->post();
        $request_list = !empty($respose) ? json_decode($respose) : array();
        //echo "<pre> hello"; print_r($request_list); print_r($firmlist); exit;
        return view('other.firm_bug_report',compact('request_list','firmlist'))->with('i');
    }
    public function viewbugreport($id,$firm)
    {
        $firmlist = DB::table('lawfirm_mgmt')->select("lawfirm_mgmt.*")
            ->where('lawfirm_mgmt.id', '=',$firm)->first();
        $url_requst = $firmlist->firm_subdomain.'/api/getbugreport';
        //$url_requst = 'http://localhost/lawware/api/getbugreport';

        $respose = Curl::to($url_requst)->withData(['id'=>$id])->post();
        $viewbug = !empty($respose) ? json_decode($respose) : array();
        return view('other.view_bug_report',compact('viewbug','firmlist'))->with('i');
    }


    public function changeStatus($id,$status)
    {

       if($status == 4)
       {
           $newStatus = 2;
       }
       if($status == 2 || $status == 3)
       {
           $newStatus = 4;
       }

       DB::table('lawfirm_mgmt')
            ->where('id', $id)
            ->update(['firm_status' => $newStatus]);

       //$firmlist = Lawfirm_mgmt::latest()->paginate(5);
       /* return view('firm.index',compact('firmlist'))
            ->with('i', (request()->input('page', 1) - 1) * 5); */
       return redirect()->route('firm')
                        ->with('success','Status updated successfully');
    }

}
