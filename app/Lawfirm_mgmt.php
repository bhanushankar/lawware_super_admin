<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lawfirm_mgmt extends Model
{
 	protected $table = 'Lawfirm_mgmt';
    protected $fillable = [
    	'firm_name',
		'firm_address',
		'firm_state',	
		'firm_country',
		'firm_city',	
		'firm_contact_number',
		'firm_fax',
		'firm_email',
		'firm_concern_person',	
		'firm_ip',	
		'firm_subdomain',	
		'firm_logo',	
		'firm_support_person',	
		'firm_support_contact',
		'firm_support_email',	
		'firm_status',
		'created_at',	
		'updated_at',
		'firm_no_of_users' => '0',
		'firm_no_of_cases' => '0',
		'firm_non_active_users' => '0',
		'firm_unique_id' => '',
		'firm_status',
		'firm_admin_username'=>'',
		'firm_admin_password'=>'',
		'firm_feature_request'=>'0',
    ];
    
}
