$(document).ready(function () {
  $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
  $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
$('#firm_country').change(function(){
    var countryID = $(this).val();
    var token = $('meta[name="csrf-token"]').attr('content');
    if(countryID){
        $.ajax({
           type:"POST",
           url:base_url+"/api/get-state-list",
           data:{country_id:countryID, _token:token},
           success:function(res){
            if(res){
                $("#firm_state").empty();
                $("#firm_state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#firm_state").append('<option value="'+value.id+'">'+value.name+'</option>');
                });

            }else{
               $("#firm_state").empty();
            }
           }
        });
    }else{
        $("#firm_state").empty();
        $("#firm_city").empty();
    }
   });
$('#firm_state').on('change',function(){
    var stateID = $(this).val();
    var token = $('meta[name="csrf-token"]').attr('content');
    if(stateID){
        $.ajax({
           type:"POST",
           url:base_url+"/api/get-city-list",
           data:{state_id:stateID, _token:token},
           success:function(res){
            if(res){
                $("#firm_city").empty();
                $.each(res,function(key,value){
                    $("#firm_city").append('<option value="'+value.id+'">'+value.name+'</option>');
                });

            }else{
               $("#firm_city").empty();
            }
           }
        });
    }else{
        $("#firm_city").empty();
    }

});
$('#firm_list').DataTable();
$('#user_list').DataTable();
$('#firm_userlog').DataTable();
$('#userlog').DataTable(
    {
        "bPaginate": true,
        "bLengthChange": true,
        "lengthMenu": [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]]
    });
$('#feature_request_list').DataTable();
$('#firm_list_delete').on('click', function (e) {
        e.preventDefault();
        //alert('in function');
        //return false;
       // setTimeout(function () {

            //   console.log(newid,casecard_no,card_no);
            swal({
                title:"Warning",
                text: "You are sure you want to delete this firm?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $('#firm_list_form').submit();
                    swal.close();
                }
            });
        //}, 800);

    });

 /* billing report chart */
var generate_bill_chart = function(ele){
  AmCharts.makeChart("bill-report", {
            "type": "serial",
            "categoryField": "category",
            "colors": [
                "#20b293",
                "#FCD202",
                "#B0DE09",
                "#0D8ECF",
                "#2A0CD0",
                "#CD0D74",
                "#CC0000",
                "#00CC00",
                "#0000CC",
                "#DDDDDD",
                "#999999",
                "#333333",
                "#990000"
            ],
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start"
            },
            "trendLines": [],
            "graphs": [{
                "balloonText": "[[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "title": "graph 1",
                "type": "column",
                "valueField": "column-1"
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "title": "Amount"
            }],
            "allLabels": [],
            "balloon": {},
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": ""
            }],
            "dataProvider": $.parseJSON(ele)
        });
}
if ( typeof bill_year_data !== 'undefined'){
generate_bill_chart(bill_year_data);}

/* generate client report chart number of client*/
var generate_client_report_chart = function(ele){
AmCharts.makeChart("client-case",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "colors": [
                        "#20b293",
                        "#FCD202",
                        "#B0DE09",
                        "#0D8ECF",
                        "#2A0CD0",
                        "#CD0D74",
                        "#CC0000",
                        "#00CC00",
                        "#0000CC",
                        "#DDDDDD",
                        "#999999",
                        "#333333",
                        "#990000"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "title": "2017"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "valueField": "column-1"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "No. of Clients"
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": ""
                        }
                    ],
                    "dataProvider": $.parseJSON(ele)
                }
            );
}
if ( typeof clinet_report_nocase !== 'undefined'){
generate_client_report_chart(clinet_report_nocase);}

/* case report line chart */
var generate_case_report_linechart = function(ele,year){
AmCharts.makeChart("linechart-case",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "colors": [
                        "#20b293",
                        "#FCD202",
                        "#B0DE09",
                        "#0D8ECF",
                        "#2A0CD0",
                        "#CD0D74",
                        "#CC0000",
                        "#00CC00",
                        "#0000CC",
                        "#DDDDDD",
                        "#999999",
                        "#333333",
                        "#990000"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "title": year
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "No. of Cases"
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": ""
                        }
                    ],
                    "dataProvider": $.parseJSON(ele)
                }
            );
}
if ( typeof case_report_nocase !== 'undefined'){
generate_case_report_linechart(case_report_nocase,(new Date()).getFullYear());}

/* generate case report pie */
var generate_case_report_piechart = function(ele){
AmCharts.makeChart("piecase-report2", {
            "type": "pie",
            "balloonText": "[[title]] : <span style='font-size:14px'><b>[[value]]</b></span>",
            "colors": [
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251",
                "#20b293",
                "#334251"
            ],
            "titleField": "category",
            "valueField": "column-1",
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider":  $.parseJSON(ele)
        });
}

if ( typeof case_report_pie !== 'undefined'){
generate_case_report_piechart(case_report_pie);}

 /* billing report table datatable */
 $('#billing-report-table').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
 /* billing list datatable */
  var billling_list_table = $('#billling_list').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                bFilter: true,
                searching: true,
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
  var outstanding_bill_list = $('#outstanding_bill_list').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                bFilter: true,
                searching: true,
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
/* open modal add bill*/
$('#open_add_billing_modal').on('click',function(){
        $('#add_billing_modal').find('input[type=text],input[type=hidden],select').val('');
        $('#total_figure').html('');
        $('.icheckbox_square-green').removeClass('checked');
        $('#add_billing_modal').modal('show');
    });
$(document.body).on('ifChanged','#dev_module',function(){
    var total_amount = parseFloat(0);
    var module_ids = '';
    $("input[name='modules[]']:checked").each( function () {
        var md_price = $(this).val().split('-');
       total_amount = parseFloat(total_amount + parseFloat(md_price[1]));
       module_ids = module_ids + md_price[0]+',';

    });
    $('#total_figure').html(total_amount);
    $('#module_ids').val(module_ids);

});
/* adding bill to db ajax */
$(document).on('click','#btn-save',function(){
    var module_fimr_name = $('#module_fimr_name').val();
    var total_amount = parseFloat($('#total_figure').html());
    var module_ids = $('#module_ids').val();
    var bill_status = $('#bill_status').val();
    var token = $('meta[name="csrf-token"]').attr('content');
    if(module_fimr_name == '0'){
        $('#billing_module_error').html('');
        $('#billing_module_error').html('Please Select Firm Name');
        return false;
    }
    if(module_ids == '0' || module_ids ==''){
        $('#billing_module_error').html('');
        $('#billing_module_error').html('Please Select Module');
        return false;
    }
    if(module_fimr_name != '0' && (module_ids != '0' || module_ids !='') )
    {
          $.ajax({
           type:"POST",
           url:base_url+"/store-bill",
           data:{
            module_fimr_name: module_fimr_name,
            total_amount :total_amount ,
            module_ids :module_ids ,
            bill_status:bill_status,
            _token:token},
           success:function(res){
           var data = $.parseJSON(res);
            if(data.success =='1'){
                window.location.href=base_url+'/billing-list';
                return false;
            }
            else if(data.success =='0'){
                $('#billing_module_error').html('');
                $('#billing_module_error').html(data.msg);
                return false;
            }
            else{

            }
           }
        });


    }

});
$(document.body).on('change','#sel1', function(){
    var $thisvalue = $(this).val();
    billling_list_table.search($thisvalue,true,false).draw();
});
$(document.body).on('change','#outsel1', function(){
    var $thisvalue = $(this).val();
    outstanding_bill_list.search($thisvalue,true,false).draw();
});
/* edit bill */
$(document.body).on('click','#billing_edit',function(){
    var id = $(this).attr('data-val');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"GET",
           url:base_url+"/show-bill/"+id,
           success:function(res){
           var data = $.parseJSON(res);
            $('#module_fimr_name').val(data.firm_id);
            $('#module_ids').val(data.module_ids);
            $('#total_figure').html(data.total_amount);
            $('#bill_status').val(data.bill_status);
            var id_module = [];
            var id_module = data.module_ids;
            $("input[name='modules[]'").prop('checked',false);
            $('.icheckbox_square-green').removeClass('checked');
            var dev_module = $("input[name='modules[]'");
            $.each(dev_module, function () {
                var checkBoxVal = $(this).val();
                var check_id = checkBoxVal.split('-');
                if ($.inArray(check_id[0], id_module) > -1) {
                $("#dev_module input[value='" + checkBoxVal + "']").prop('checked', true);
                $(this).parents('.icheckbox_square-green').addClass('checked');
                }
            });
            $('#add_billing_modal').modal('show');
           }
        });
});
/* resend outstanding bill */
$(document.body).on('click','#out_resend',function(){
    var firm_id = $(this).attr('data-firm');
    var bill_id = $(this).attr('data-bill');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/resendinvoice",
           data : {firm_id:firm_id,bill_id:bill_id},
           success:function(res){
           var data = $.parseJSON(res);

           }
        });
});
/*get pdf biling*/
$(document.body).on('click','#bill_pdf',function(){
    var firm_id = $(this).attr('data-firm-pdf');
    var bill_id = $(this).attr('data-bill-pdf');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/invoicepdf",
           data : {firm_id:firm_id,bill_id:bill_id},
           success:function(res){
           var link = document.createElement("a");
           link.download = 'invoice';
           link.href = res;
           link.click();
           }
        });
});
$(document.body).on('change','.select-client, #sel_year',function(){

    var firm_id = $('.select-client').val();
    var year = $('#sel_year').val();
    var ajax_req = 'fromajax';
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/case-report-ajax",
           data : {firm_id:firm_id,year:year,ajax_req:ajax_req,_token:token},
           success:function(res){
           var res = $.parseJSON(res);
           if (res.total_count != 0){
               generate_case_report_linechart(res.case_report_nocase,year);
               generate_case_report_piechart(res.case_report_pie);
               $('#report_nocase').html(res.total_count);
               $('#year_set').html(year);
               $('#year_set1').html(year);
               return false;
            }
            else{
            swal({
                title: "Alert",
                text: "No Data Found.",
                type: "warning"
            });
            return false;
            }
           }
        });
});
$(document.body).on('change','.select-client-bill, #sel_year_bill',function(){

    var firm_id = $('.select-client-bill').val();
    var year = $('#sel_year_bill').val();
    var ajax_req = 'fromajax';
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/billing-report-ajax",
           data : {firm_id:firm_id,year:year,ajax_req:ajax_req,_token:token},
           success:function(res){
           var res = $.parseJSON(res);
           if (res.total_count != 0){
               generate_bill_chart(res.bill_data_yearly);
               //$('#report_nocase').html(res.total_count);
              // $('#year_set').html(year);
               //$('#year_set1').html(year);
               return false;
            }
            else{
            swal({
                title: "Alert",
                text: "No Data Found.",
                type: "warning"
            });
            return false;
            }
           }
        });
});
$(document.body).on('click','#view_scl',function(){

    var firm_id = $(this).attr('scl-data');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/view-setupchecklist",
           data : {firm_id:firm_id,_token:token},
           success:function(res){
           var res = $.parseJSON(res);
           if (res.status == 1){
                console.log(res.data);
               $('#scl_modal_body').html('');
               $('#scl_firm_name').html('');
               $('#scl_firm_name').html(res.firm_name+' Setup Checklist '+res.percent+'%');
               $('#scl_modal_body').html(res.data);
               $('#scl_modal').modal('show');
               return false;
            }
            else{
            swal({
                title: "Alert",
                text: "No Data Found. OR Due to Domain is not publicly accessible!!",
                type: "warning"
            });
            return false;
            }
           }
        });
});

$(document.body).on('click','#login_admin',function(){
    var firm_id = $(this).attr('data-id');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
           type:"POST",
           url:base_url+"/logintolawware",
           data : {firm_id:firm_id,_token:token},
           success:function(res){
           var res = $.parseJSON(res);
           if (res.status == 1){
                console.log(res);
                alert(res.url_request);
                $('#super_to_admin').attr('action',res.url_request);
                $('#admin_username').val('lawware');
                $('#admin_password').val('law2017');
                $('#super_to_admin').submit();
              window.open('http://localhost/lawware/admin/dashboard','_blank');
               return false;
            }
            else{
            swal({
                title: "Alert",
                text: "No Data Found. Due to Domain is not publicly accessible!!",
                type: "warning"
            });
            return false;
            }
           }
        });
});
/*document ready ends */
});