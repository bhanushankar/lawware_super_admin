<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8" />
    <title>Outstanding Bill</title>
</head>

<body style="font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 0; margin: 0;">
 <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 5px;vertical-align: middle;padding-bottom: 20px;">
            <img src="{{asset('/img/lawware-logo.jpg')}}" width="100px">
        </td>

        <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 20px;">
            
            Invoice #: OutStandig <br>
            Created: {{date('d F Y',strtotime($resendinvoice->start_date))}}<br>
            Due: {{date('d F Y',strtotime($resendinvoice->start_date))}}
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 5px;vertical-align: top;padding-bottom: 40px;">
            Lawware, Inc.<br>
            12345 Sunny Road<br>
            Sunnyville, CA 12345
        </td>

        <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 40px;">
            {{$resendinvoice->firm_name}}<br>
            {{$resendinvoice->firm_contact_number}}<br>
            {{$resendinvoice->firm_email}}
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%">

    <tr>
        <td style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
            Item
        </td>

        <td style="padding: 5px;vertical-align: top;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
            Price
        </td>
    </tr>
    @foreach($dev_modules as $dm)
    @if(in_array($dm->id, explode(',',$resendinvoice->module_ids)))
    <tr>
        <td style="padding: 5px;vertical-align: top;border-bottom: 1px solid #eee;">
            {{$dm->module_name}}
        </td>

        <td style="padding: 5px;vertical-align: top;text-align: right;border-bottom: 1px solid #eee;">
            ${{$dm->module_price}}
        </td>
    </tr>
    @endif
    @endforeach
    <tr>
        <td style="padding: 5px;vertical-align: top;"></td>

        <td style="padding: 5px;vertical-align: top;text-align: right;border-top: 2px solid #eee;font-weight: bold;">
         Total: ${{$resendinvoice->total_amount}}
     </td>
 </tr>
</table>
</body>
</html>