@extends('layouts.master')

@section('content')
    <div class="row">   
<div class="col-md-12">
            <div class="panel panel-default firm-list">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Firm</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('firm') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('route' => 'firm-store','method'=>'POST','files' => true)) !!}
         @include('firm.form')
    {!! Form::close() !!}
</div></div></div>
@endsection