@extends('layouts.master')

@section('content')
    <div class="row">   
<div class="col-md-12">
            <div class="panel panel-default firm-list">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Firm List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('firm-show',$id) }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="firm_list">
        <thead>
        <tr>
            <th>No</th>
            <th>Case Number</th>
            <th>Case Status</th>
            <th>Open Date</th>
            <th>Close Date</th>
            <th>Caption</th> 
            <th>Case Status</th> 
            <th width="280px">Action</th>
        </tr>
        </thead>
    <tbody>
    @if(!empty($caselist))
    @foreach ($caselist as $article)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $article->caseno}}</td>
        <td>{{ $article->casestat}}</td>
        <td>{{ $article->dateopen}}</td>
        <td>{{ $article->dateclosed}}</td>
        <td>{{ $article->caption1}}</td>
        <td>{{ $article->case_status}}</td>
        <td>
        </td>
     </tr>
     @endforeach
     @else
     <tr><td colspan="8" style="text-align: center;">No Data Found</td></tr>
     @endif
    </tbody>
    </table>
</div>
</div></div></div>
@endsection