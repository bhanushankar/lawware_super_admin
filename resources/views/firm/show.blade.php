@extends('layouts.master')

@section('content')
 <div class="row">   
<div class="col-md-12">
            <div class="panel panel-default firm-list">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Firm Details</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('firm') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
                <div class="form-group">
                    <img src="{{asset('/firm_logo/' . $firmlist->firm_logo)}}" height="150" width="150">
                </div>
            </div>
            <div class="col-md-10">
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Firm:</strong></label>
                    {{ $firmlist->firm_name}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Address:</strong></label>
                    {{ $firmlist->firm_address}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Country:</strong></label>
                    {{ $firmlist->country}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>State:</strong></label>
                    {{ $firmlist->state}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>City:</strong></label>
                    {{ $firmlist->city}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Firm Contact:</strong></label>
                    {{ $firmlist->firm_contact_number}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Fax:</strong></label>
                    {{ $firmlist->firm_fax}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Firm Email:</strong></label>
                    {{ $firmlist->firm_email}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Firm Concern Person:</strong></label>
                    {{ $firmlist->firm_concern_person}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Firm IP:</strong></label>
                    {{ $firmlist->firm_ip}}
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Support Person:</strong></label>
                    {{ $firmlist->firm_support_person}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Support Person Contact:</strong></label>
                    {{ $firmlist->firm_support_contact}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                   <label> <strong>Support Person Email:</strong></label>
                    {{ $firmlist->firm_support_email}}
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                   <label> <strong>Date:</strong></label>
                    {{ $firmlist->created_at}}
                </div>
            </div>

            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label><strong>Public Domain:</strong></label>
                    {{ $firmlist->firm_subdomain}}
                </div>
            </div>
            @if(!empty($userlist))
            <div class="col-sm-6 col-md-4">
             <label> <strong>Number of users</strong></label> : {{count($userlist)}}
            </div>
            @endif
            @if(!empty($casecount))
            <div class="col-sm-6 col-md-4">
            <label> <strong>Number of cases</strong></label> : {{$casecount}}
            </div>
            @endif
        </div>
    </div>
</div></div></div>
@if(!empty($userlist))
<div class="row">   
<div class="col-md-12">
            <div class="panel panel-default firm-list">
    <div class="row">
        <div class="col-md-12 table-responsive">
        <table class="table table-bordered table-hover dataTables-example" id="user_list">
        <thead>
            <tr>
                <th>Sr no</th>
                <th>Initials</th>
                <th>Username</th>
                <th>Name</th>
                <th>Title</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($userlist as $article)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $article->initials}}</td>
        <td>{{ $article->username}}</td>
        <td>{{ $article->fname}} {{$article->lname }}</td>
        <td>{{ $article->title}}</td>
        <td>{{ $article->emailid}}</td>
        <td><a class="btn btn-primary" href="{{ route('user-cases',['user'=>$article->initials,'id'=>$firmlist->id]) }}">Show Case(s)</a></td>
     </tr>
    @endforeach
        </tbody>
    </table>
        </div>
    </div>
</div></div></div>
@endif
@endsection