<div class="row blade-form">    
    <div class="col-md-6">
        <div class="form-group">
            <strong>Logo:</strong>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select logo</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="firm_logo" id="firm_logo"/>
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
            <!-- {!! Form::file('firm_logo', array('class' => 'form-control','id'=>'firm_logo')) !!} -->
        </div>
    </div>
    @if(isset($firmlist))
    <div class="col-md-4">
        <img src="{{ asset('/firm_logo/' . $firmlist->firm_logo)}}" width="80">
    </div> 
    @endif
</div>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Firm Name:</strong>
            {!! Form::text('firm_name', null, array('placeholder' => 'Firm Name','class' => 'form-control','id'=>'firm_name')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Firm Address:</strong>
            {!! Form::text('firm_address', null, array('placeholder' => 'Firm Address','class' => 'form-control' ,'id'=>'firm_address')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Country:</strong>
            <select name="firm_country" id="firm_country" class="form-control" style="">
                @foreach($countries as $item)
                <option value="{{$item->id}}" {{ (isset($country_id) && $country_id == $item->id) ? 'selected="selected"' : ''}}>{{$item->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>State:</strong>
            @if(isset($state))
            <select name="firm_state" id="firm_state" class="form-control" style="">
                @foreach($state as $item)
                <option value="{{$item->id}}" {{ (isset($state_id) && $state_id == $item->id) ? 'selected="selected"' : ''}}>{{$item->name}}</option>
                @endforeach
            </select>
            @else
            <select name="firm_state" id="firm_state" class="form-control" style="">
            </select>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>City:</strong>
            @if(isset($city))
            <select name="firm_city" id="firm_city" class="form-control" style="">@foreach($city as $item)
                <option value="{{$item->id}}" {{ (isset($city_id) && $city_id == $item->id) ? 'selected="selected"' : ''}}>{{$item->name}}</option>
                @endforeach
            </select>
            @else
            <select name="firm_city" id="firm_city" class="form-control" style="">
            </select>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Contact Number:</strong>
            {!! Form::text('firm_contact_number', null, array('placeholder' => 'Contact Number of firm','class' => 'form-control' ,'id'=>'firm_contact_number')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Fax:</strong>
            {!! Form::text('firm_fax', null, array('placeholder' => 'Fax number of firm','class' => 'form-control','id'=>'firm_fax')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Status:</strong>
            <select name="firm_status" id="firm_status" class="form-control" style="">
                <option value="1" {{ (isset($firmlist->firm_status) && $firmlist->firm_status=='1') ? 'selected="selected"' : ''}}>Inquiry</option>
                <option value="2" {{ (isset($firmlist->firm_status) && $firmlist->firm_status=='2') ? 'selected="selected"' : ''}}>Confirm</option>
                <option value="3" {{ (isset($firmlist->firm_status) && $firmlist->firm_status=='3') ? 'selected="selected"' : ''}}>Outstanding / Followup</option>
                <option value="4" {{ (isset($firmlist->firm_status) && $firmlist->firm_status=='4') ? 'selected="selected"' : ''}}>In-active</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('firm_email', null, array('placeholder' => 'Email of firm','class' => 'form-control','id'=>'firm_email')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Concern Person:</strong>
            {!! Form::text('firm_concern_person', null, array('placeholder' => 'Concern Person of firm','class' => 'form-control' ,'id'=>'firm_concern_person')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Firm IP:</strong>
            {!! Form::text('firm_ip', null, array('placeholder' => '127.0.0.1','class' => 'form-control', 'id'=>'firm_ip','pattern'=>'((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Public Domain:</strong>
            {!! Form::text('firm_subdomain', null, array('placeholder' => 'Public Domain of firm','class' => 'form-control','id'=>'firm_subdomain')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">             
        <div class="form-group">
            <strong>Technical Support Person:</strong>
            {!! Form::text('firm_support_person', null, array('placeholder' => 'Technical Support Person','class' => 'form-control','id'=>'firm_support_person')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Technical Support Contact:</strong>
            {!! Form::text('firm_support_contact', null, array('placeholder' => 'Technical Support Contact','class' => 'form-control','id'=>'firm_support_contact')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Technical Support Email:</strong>
            {!! Form::text('firm_support_email', null, array('placeholder' => 'Technical Support Email','class' => 'form-control','id'=>'firm_support_email')) !!}
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
