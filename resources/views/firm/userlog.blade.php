@extends('layouts.master')

@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>{{$firmlist->firm_name}} - User log</h2>
                        </div>
                    </div>
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="userlog">
        <thead>
        <tr>
            <th>No</th>
            <th>Initials</th>
            <th>Signon</th>
            <th>Signoff</th> 
        </tr>
        </thead>
        <tbody>
        @foreach ($request_list as $article)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $article->initials}}</td>
            <td>{{ $article->signon}}</td>
            <td>{{ $article->signoff}}</td>
         </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div></div></div>
@endsection