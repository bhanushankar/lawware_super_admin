@extends('layouts.master')

@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Firm List</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('firm-create') }}"> Setup New Firm</a>
                        </div>
                    </div>
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="firm_list">
        <thead>
        <tr>
            <th>No</th>
            <th>Firm name</th>
            <th>Firm address</th>
            <!-- <th>Firm country</th>
            <th>Firm state</th>
            <th>Firm city</th>    -->
            <th>Firm contact number</th>
            <!-- <th>Firm fax</th> -->
            <th>Firm email</th>
            <th>concern person</th>
            <th>Firm IP</th>
            <th>Public domain</th>
            <!-- <th>Firm logo</th>  -->
            <!-- <th>Support person</th>
            <th>Support contact</th>
            <th>Support email</th> -->
          <!--   <th>Date</th>  -->
            <th style="min-width: 165px; max-width: 165px;">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($firmlist as $article)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $article->firm_name}}</td>
            <td>{{ $article->firm_address}}</td>
           <!--  <td>{{ $article->firm_country}}</td>
            <td>{{ $article->firm_state}}</td>
            <td>{{ $article->firm_city}}</td> -->
            <td>{{ $article->firm_contact_number}}</td>
            <!-- <td>{{ $article->firm_fax}}</td> -->
            <td>{{ $article->firm_email}}</td>
            <td>{{ $article->firm_concern_person}}</td>
            <td>{{ $article->firm_ip}}</td>
            <td>{{ $article->firm_subdomain}}</td>
            <!-- <td><img src="{{ asset('/firm_logo/' . $article->firm_logo)}}" width="80" height="50" /></td> -->
           <!--  <td>{{ $article->firm_support_person}}</td>
            <td>{{ $article->firm_support_contact}}</td>
            <td>{{ $article->firm_support_email}}</td> -->
            <!-- <td>{{ $article->created_at}}</td> -->
            <td>
                <a class="btn btn-info" href="{{ route('firm-show',$article->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('firm-edit',$article->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['firm-destroy', $article->id],'id'=>'firm_list_form','style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger' ,'id'=>'firm_list_delete']) !!}
                {!! Form::close() !!}

                {{ Form::open(array('url' => $article->firm_subdomain.'/admin/login','target'=>"_blank",'autocomplete'=>"off")) }}
                @if($article->firm_status != 1)
                    <a class="btn btn-primary" href="{{ route('firm-status',$article->id.'/'.$article->firm_status) }}">
                        @if($article->firm_status == 4)
                        ACTIVE
                     @else
                        INACTIVE
                     @endif
                    </a>
                @endif
                <input type="text" style="display: none;" id="admin_username" name="admin_username" value="lawware" class="form-control valid" >
                <input name="admin_password" style="display: none;" id="admin_password" type="password" class="form-control valid" value="law2017" >
                {!! Form::submit('Login', ['class' => 'btn btn-danger' ]) !!}
                {{ Form::close() }}
            </td>
         </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div></div></div>

    {!! $firmlist->links() !!}
@endsection