@extends('layouts.master')

@section('content')
   <div class="row">   
<div class="col-md-12">
            <div class="panel panel-default firm-list">
             <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Article</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('firm') }}"> Back</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($firmlist, ['method' => 'PATCH','route' => ['firm-update', $firmlist->id] ,'files' => true]) !!}
        @include('firm.form')
    {!! Form::close() !!}
</div></div></div>

<script type="text/javascript">
$(document).ready(function () {
    setTimeout(function()
    {
        $('#firm_country').trigger("change");
    
    },2000);
});
</script>
@endsection