<!DOCTYPE html>
<html lang="{{ app()->getLocale() }} class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Lawware | Super Admin | Dashboard</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/chosen/chosen.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/summernote/summernote.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/inspinia.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom-sa.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
        <script src="{{ asset('js/jquery-ui-1.10.4.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </head>
    <body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="">
                            <img src="{{asset('/img/lawware-logo.jpg')}}" width="100px">
                        </div>
                    </li>
                                           
                    <li>
                        <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="{{ route('firm') }}"><i class="fa fa-database"></i> <span class="nav-label">Law Firms</span></a>
                    </li>
                    <li>
                        <a href="{{ route('billing-list') }}"><i class="fa fa-database"></i> <span class="nav-label">Billing</span></a>
                    </li>
                    <li>
                        <a href="{{ route('setupchecklist') }}"><i class="fa fa-calendar-check-o"></i> <span class="nav-label">Law Frim Checklist</span></a>
                    </li>
                    <li>
                        <a href="{{route('feature-request')}}"><i class="fa fa-list-ol"></i> <span class="nav-label">Feature Request List</span></a>
                    </li>
                    <li class="">
                        <a href="#">
                            <i class="fa fa-table"></i>
                            <span class="nav-label">Reports</span>
                            <span class="fa arrow"></span>
                        </a>
                            <ul class="nav nav-second-level ">
                            <li>
                                <a href="{{route('billing-report')}}">Billing Report</a>
                            </li> 
                            <li>
                                <a href="{{route('outstanding-bills')}}">Outstanding Bills</a>
                            </li>
                            <li class="">
                                <a href="{{route('case-report')}}">Case Report</a>
                            </li>
                            <li class="">
                                <a href="{{route('userlog-list')}}">User Login Report</a>
                            </li>
                            <li>
                                <a href="{{route('bug-report-list')}}">Bug Reports</a>
                            </li>
                            <li>
                                <a href="{{route('client-report')}}">Client Report</a>
                            </li>
                            <li>
                                <a href="{{route('setupchecklist')}}">Setup Checklist</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
              <!--   <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div> -->
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to Lawware Super Admin Panel</span>
                </li>
                @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i>Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
               
            </ul>

        </nav>
    </div>
<div class="wrapper wrapper-content">
     @yield('content')
</div>
    <div class="footer">
            
            <div>
                <strong>Copyright</strong> LawWare &copy; 2016-{{ Date('Y')}}
            </div>
        </div>
<!-- biling modal dilog box -->
<div class="modal fade" id="add_billing_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Select Module :</h4>
            </div>
            <div class="modal-body">
                <div class="row mar-bot20">
                
                    @if(isset($firm_bill_list) && !empty($firm_bill_list))
                    <div class="col-sm-4 col-md-4"><label><h3>Select Firm Name :</h3></label></div>
                    <div class="col-sm-6 col-md-6"><select id="module_fimr_name" class="form-control col-sm-3">
                        <option value="0" selected="selected">Select Firm</option>
                        @foreach($firm_bill_list as $fbl)
                        <option value="{{$fbl->id}}">{{$fbl->firm_name}}</option>
                        @endforeach
                    </select>
                </div>
                    @endif
                
            </div>
            <div class="row mar-bot20">
            <div class="form-group">
                @if(isset($dev_module_list) && !empty($dev_module_list))
                <div class="col-sm-4 col-md-4"><h3>Modules :</h3></div>
                 <div class="col-sm-6 col-md-6">
                    @foreach($dev_module_list as $dml)
                    <div class="i-checks"><label class=""> <div class="icheckbox_square-green " style="position: relative;">
                            <input type="checkbox" checked="" value="{{$dml->id}}-{{$dml->module_price}}" name="modules[]" id="dev_module" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> {{$dml->module_name}} &nbsp; ${{$dml->module_price}}</label>
                   </div>
                   @endforeach
           </div>
           @endif
       </div>
       </div>
       <div class="row mar-bot20">
        <div class="col-sm-4 col-md-4"><label><h3>Status :</h3></label></div>
        <div class="col-sm-3 col-md-3">
        <div class="form-group">
        <select id="bill_status" name="bill_status" class="form-control col-sm-3">
            <option value="1">Pending</option>
            <option value="2">Confirm</option>
            <option value="3">Deactivate</option>
        </select>  
        </div>
    </div>
       </div>
        <div class="row ml-1">
         <div class="col-sm-4 col-md-4"><h3>Total Amount:</h3></div>
         <div class="col-sm-4 col-md-4"><h3>$<span id="total_figure"></span></h3></div>
         <input type="hidden" id="module_ids" name="module_ids" value="0"/>
        </div>
        <div id="billing_module_error" style="color: red; font-size: 14px;"></div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-save" >Submit</button>
            <input type="hidden" id="product_id" name="product_id" value="0">
            </div>
        </div>
      </div>
  </div>
  <!-- billing modal ends here -->
  <!-- setup checklist start from here -->
  <div id="scl_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="scl_firm_name"></h4>
      </div>
      <div class="modal-body">
        <div id="scl_modal_body"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- setupchecklist modal ends here -->
        </div>
    </div>
    <script type="text/javascript">
        var base_url = 'http://localhost/super_lawware/public';
    </script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <!-- jQuery UI -->
    <!--<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>-->
    <!-- GITTER -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    <!-- Jvectormap -->
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- EayPIE -->
    <script src="{{ asset('js/plugins/easypiechart/jquery.easypiechart.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>
    <!-- Jasny -->
    <script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    <!-- jqGrid -->
    <script src="{{ asset('js/plugins/jqGrid/i18n/grid.locale-en.js') }}"></script>
    <script src="{{ asset('js/plugins/jqGrid/jquery.jqGrid.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jeditable/jquery.jeditable.js') }}"></script>
    <script src="{{ asset('js/plugins/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('js/plugins/dataTables/buttons.print.min.js')}}"></script>
    <script src="{{ asset('/js/plugins/dataTables/dataTables.select.min.js')}}"></script>
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ asset('js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- Data picker -->
    <script src="{{ asset('js/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/plugins/cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fullcalendar/moment.min.js') }}"></script>
    <script src="{{ asset('js/plugins/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
    <script src="{{ asset('js/plugins/datetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/codemirror/codemirror.js') }}"></script>
    <script src="{{ asset('js/plugins/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/amcharts/amcharts.js') }}"></script>
    <script src="{{ asset('js/amcharts/serial.js') }}"></script>
    <script src="{{ asset('js/amcharts/pie.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    