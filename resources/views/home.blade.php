@extends('layouts.master')

@section('content')
<div class="dashboard">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-sm-12">
                        <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bank fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$firmcount}}</div>
                                    <div><h3>Firms</h3></div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('firm') }}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-address-book-o fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$firm_no_of_users}}</div>
                                    <div><h3>Users!</h3></div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('firm') }}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-id-card-o fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $firm_no_of_inquiry}}</div>
                                    <div><h3>New Lead!</h3></div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><h5>0</h5></div>
                                    <div><h4>Support Tickets</h4></div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
                    </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins ">
                    <div class="ibox-title">
                        <h5>Quick Links</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('billing-list') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-database"></i>
                                        <span>Billing</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('setupchecklist') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-calendar-check-o"></i>
                                        <span>Firm Checklist</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('feature-request') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-list-alt"></i>
                                        <span>Feature Requested</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('billing-report') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Billing Reports</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('outstanding-bills') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Outstanding Invoice</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('case-report') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Case Reports</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('userlog-list') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Login Reports</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('bug-report-list') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Bug Reports</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <a href="{{ route('client-report') }}" target="_blank">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Client Reports</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                     <!--    <div class="row">
                            <div class="col-lg-3 col-sm-3">
                                <a href="#">
                                    <div class="ibox-content box">
                                        <i class="fa fa-bell"></i>
                                        <span>Reminders</span>
                                    </div>
                                </a>
                            </div>
                            
                            
                        </div> -->
                    </div>
                </div>
            </div>
            


                        <!-- -------------------------------------------------------------- -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
