@extends('layouts.master')

@section('content')
     
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Billing List</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Billing List</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                  <!--   <ul class="dropdown-menu dropdown-user">
                                        <li>
                                            <a href="#">Config option 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a> -->
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row mar-bot20">
                                    <div class="col-sm-10">
                                        <form action="" class="form-inline">
                                            <div class="form-group">
                                                <select class="form-control col-sm-3" id="sel1"  data-search="">
                                                    <option value="0">Firm Name</option>
                        @foreach($firm_bill_list as $fbl)
                        <option value="{{$fbl->firm_name}}">{{$fbl->firm_name}}</option>
                        @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control col-sm-3" id="sel1">
                                                    <option value="0">Month </option>
                                                     @foreach($available_months as $abl)
                        <option value="{{$abl->month}}">{{$abl->month}}</option>
                        @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control col-sm-3" id="sel1">
                                                    <option value="0">Status </option>
                                                    <option>Pending </option>
                                                    <option>Confirm </option>
                                                    <option>Deactivate </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-2">
                                        <button id="open_add_billing_modal" class="btn btn-primary pull-right">Add Bill</button>
                                    </div>

                                </div>
                                <div class="table-responsive billing-table1">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" id="billling_list">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Firm Name</th>
                                                <th>Month</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Download Invoice</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($billing_list))
                                            @foreach($billing_list as $bl)
                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{$bl->firm_name}}</td>
                                                <td>{{date('F Y',strtotime($bl->created_at))}}</td>
                                                <td>${{$bl->total_amount}}</td>
                                                
                                                <td>@if($bl->bill_status == '1') Pending
                                                @elseif($bl->bill_status == '2') Confirm
                                                @else Deactivate
                                                @endif</td>
                                                <td>
                                                    <a href="javascript:void(0);" id="bill_pdf" data-firm-pdf="{{$bl->id}}" data-bill-pdf="{{$bl->id}}">Download</a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-xs btn-right" id="billing_edit" data-val="{{$bl->id}}">Edit</button>
                                                   <!--  <button type="button" class="btn btn-danger btn-xs" id="billing_delete" data-val="{{$bl->id}}">Delete</button> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                             <tr>
                                                <td colspan="7" class="text-center">No Data Found.</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
 