@extends('layouts.master')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Billing Report</h2>
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Billing Report Chart</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-inline pull-right">
                                    <div class="form-group">
                                        <label>Select Client:</label>
                                        <select class="form-control select-client-bill">
                                          @foreach($firmlist_selectbox as $fbl)
                        <option value="{{$fbl->id}}" {{($fbl->id == 1)?'selected="selected"':''}}>{{$fbl->firm_name}}</option>
                        @endforeach
                                        </select>
                                    </div>
                                     <div class="form-group">
                                        <label>Select Year:</label>
                                    <select id="sel_year_bill" class="form-control" name="sel_year_bill">
                                        @for($i=$years_start; $i<=$current_year; $i++)
                                        <option value="{{$i}}" {{($i == date('Y'))?'selected="selected"':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                </form>
                                <div id="bill-report" style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Billing List</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive billing-table2">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" id="billing-report-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Firm Name</th>
                                                <th>Contact</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Month</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($bill_undert_this_month) && !empty($bill_undert_this_month))
                                            @foreach($bill_undert_this_month as $bm)
                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{$bm->firm_name}}</td>
                                                <td>{{$bm->firm_contact_number}}</td>
                                                <td>{{$bm->total}}</td>
                                                @if($bm->bill_status == '1')
                                                <td class="text-danger"> Not Paid
                                                @elseif($bm->bill_status == '2')
                                                <td class="text-Success"> Paid
                                                @else 
                                                <td class="text-danger">Deactivate
                                                @endif
                                                </td>
                                                <td>{{$bm->month}}</td>
                                            </tr>
                                           @endforeach
                                           @else
                                           <tr><td colspan="6" class="text-center">No Data Found.</td> 
                                           </tr> 
                                           @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var bill_year_data = '<?php echo $bill_data_yearly;?>';
            </script>
 @endsection        
    
    
