@extends('layouts.master')

@section('content')
     
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Billing List</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Billing List</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                  <!--   <ul class="dropdown-menu dropdown-user">
                                        <li>
                                            <a href="#">Config option 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a> -->
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row mar-bot20">
                                    <div class="col-sm-10 col-md-10">
                                        <form action="" class="form-inline">
                                            <div class="">
                                                <div class="col-sm-2 col-md-2"><h4>Select Month</h4></div>
                                                <div class="col-sm-4 col-md-4">
                                                    <select class="form-control col-sm-3" id="outsel1">
                                                    <option value="0">Month </option>
                                                    <option value='Janaury'>Janaury</option>
                                                    <option value='February'>February</option>
                                                    <option value='March'>March</option>
                                                    <option value='April'>April</option>
                                                    <option value='May'>May</option>
                                                    <option value='June'>June</option>
                                                    <option value='July'>July</option>
                                                    <option value='August'>August</option>
                                                    <option value='September'>September</option>
                                                    <option value='October'>October</option>
                                                    <option value='November'>November</option>
                                                    <option value='December'>December</option>
                                                </select>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive billing-table1">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" id="outstanding_bill_list">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Firm Name</th>
                                                <th>Due Date</th>
                                                <th>Due Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($outstanding_bill))
                                            @foreach($outstanding_bill as $bl)
                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{$bl->firm_name}}</td>
                                                <td>{{date('d F Y',strtotime($bl->end_date))}}</td>
                                                <td>${{$bl->total_amount}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-xs btn-right" id="out_resend" data-firm="{{$bl->id}}" data-bill="{{$bl->bill_id}}">Resend Invoice</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                             <tr>
                                                <td colspan="5" class="text-center">No Data Found.</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
 