@extends('layouts.master')

@section('content')
			<table class='table'>
            <tr>
            <td class='text-info' width='30%'>Summary :</td>
            <td>
            {{$viewbug->summary}}
            </td>
            </tr>
            <tr>
            <td class='text-info'>Descriptions:</td>
            <td type='text/html'>
           	<?php echo html_entity_decode($viewbug->descriptions);?></pre>
            </td>
            </tr>

            <tr>
            <td class='text-info'>Reported By Firm :</td>
            <td>
           {{ $viewbug->repoter}}
            </td>
            </tr>

            <tr>
            <td class='text-info'>Module :</td>
            <td>
           {{ $viewbug->module}}
            </td>
            </tr>

            <tr>
            <td class='text-info'>Sub Module :</td>
            <td>
            {{$viewbug->submodule}}
            </td>
            </tr>

            <tr>
            <td class='text-info'>Image :</td>
            <td>
            <img src="{{$firmlist->firm_subdomain}}/{{$viewbug->images}}" width='600px'>
            </td>
            </tr>

            <tr>
            <td class='text-info'>Status :</td>
            <td>
            @if($viewbug->status == 0) Pending 
            @elseif($viewbug->status == 1) Viewed 
            @elseif($viewbug->status == 2) Resolved 
            @else On Hold 
            @endif
            </td>
            </tr>
            <tr>
            <td class='text-info'>Posted on:</td>
            <td>
            {{date('m-d-Y H:i',strtotime($viewbug->datecreated))}}
            </td>
            </tr>
</table>
@endsection