@extends('layouts.master')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Case Report</h2>
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Case Report Chart <small id="year_set">2017</small> </h5> 
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                  <form class="form-inline pull-right">
                                    <div class="form-group">
                                        <label>Select Client:</label>
                                        <select class="form-control select-client">
                                          @foreach($firmlist_selectbox as $fbl)
                        <option value="{{$fbl->id}}" {{($fbl->id == 1)?'selected="selected"':''}}>{{$fbl->firm_name}}</option>
                        @endforeach
                                        </select>
                                    </div>
                                     <div class="form-group">
                                        <label>Select Year:</label>
                                    <select id="sel_year" class="form-control" name="sel_year">
                                        @for($i=$years_start; $i<=$current_year; $i++)
                                        <option value="{{$i}}" {{($i == date('Y'))?'selected="selected"':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                </form>
                                <div class="total-case">
                                    <p>Total No of cases:
                                        <span id="report_nocase">{{$total_count}}</span>
                                    </p>
                                </div>
                                <div id="linechart-case" style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight" style="padding-bottom: 5px !important;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Case Report Chart
                                    <small id="year_set1">2017</small>
                                </h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                              
                                <div id="piecase-report2" style="width: 100%; height: 400px; background-color: #FFFFFF;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var case_report_nocase = '<?php echo $case_report_nocase;?>';
                var case_report_pie = '<?php echo $case_report_pie;?>';
            </script>
@endsection 