@extends('layouts.master')

@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Bug Report List</h2>
                        </div>
                    </div>
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="firm_userlog">
        <thead>
        <tr>
            <th>No</th>
            <th>Firm name</th>
            <th>Summary</th>
            <th>Module</th>
            <th style="min-width: 45px; max-width: 45px;">Status</th>
            <th>Date</th>
            <th style="min-width: 35px; max-width: 35px;">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($request_list as $article)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $article->repoter}}</td>
            <td>{{ $article->summary}}</td>
            <td>{{ $article->module}}</td>
            <td>{{ $article->status}}</td>
            <td>{{ date('m-d-Y H:i',strtotime($article->datecreated))}}</td>
            <td>
                <a class="btn btn-info" href="{{ route('view-bug-report',[$article->id,$firmlist->id]) }}" id="view_bug" bug-data="{{$article->id}}">View </a>
            </td>
         </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div></div></div>
@endsection