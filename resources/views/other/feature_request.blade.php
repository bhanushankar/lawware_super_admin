@extends('layouts.master')

@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Firm Wise Feature(s) Request List</h2>
                        </div>
                       <!--  <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('firm-create') }}"> Setup New Firm</a>
                        </div> -->
                    </div>
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="feature_request_list">
        <thead>
        <tr>
            <th>No</th>
            <th>Firm name</th>
            <th>Firm address</th>
            <th>Firm contact number</th>
            <th>Firm email</th>
            <th>Concern person</th> 
            <th>Public domain</th>  
            <th>Feature Request</th>  
            <th style="min-width: 165px; max-width: 165px;">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($firmlist as $article)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $article->firm_name}}</td>
            <td>{{ $article->firm_address}}</td>
            <td>{{ $article->firm_contact_number}}</td>
            <td>{{ $article->firm_email}}</td>
            <td>{{ $article->firm_concern_person}}</td>
            <td>{{ $article->firm_subdomain}}</td>
            <td><b>{{ $article->firm_feature_request}}</b></td>
            <td>
                <a class="btn btn-info" href="{{ route('view-feature-request',$article->id) }}">View Request</a>
            </td>
         </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div></div></div>

@endsection