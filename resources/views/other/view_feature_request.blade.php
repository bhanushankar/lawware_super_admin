@extends('layouts.master')

@section('content')
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>View Feature(s) Request From   {{$firmlist->firm_name}}</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('feature-request') }}"> Back</a>
                        </div>
                    </div>
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row">
    <div class="col-md-11">
    @foreach ($feature_request_list as $article)
        <table class="table" width="75%">
            <tr>
                <td width="25%">{{ ++$i }}</td>
                <td></td>
            </tr>
            <tr>
                <td>Module Name : </td>  
                <td>{{ $article->moduleName }}</td>
            </tr>
            <tr>
                <td>Sub Module Name : </td>  
                <td>{{ $article->subModuleName }}</td>
            </tr>
            <tr>
                <td>Feature(s) Request</td>
                <td><pre>{{ $article->featureRequest }}</pre></td>
            </tr>
        </table>  
        @endforeach
       </div> 
   </div>
</div></div></div>

@endsection