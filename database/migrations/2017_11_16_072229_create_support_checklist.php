<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_checklist', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('install_xampp');
            $table->tinyInteger('code');
            $table->tinyInteger('database_migration');
            $table->tinyInteger('chat_server');
            $table->tinyInteger('created_users');
            $table->string('public_domain',50);
            $table->string('ip_address',20);
            $table->string('firm_name',30);
            $table->integer('firm_id');
            $table->string('unique_no',50);
            $table->dateTime('created_date');
            $table->dateTime('updated_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_checklist');
    }
}
