<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToLawfirmMgmt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('lawfirm_mgmt', function($table)
        {
            $table->integer('firm_no_of_users');
            $table->integer('firm_no_of_cases');
            $table->integer('firm_non_active_users');
            $table->string('firm_unique_id');
            $table->enum('firm_status',array('1','2','3','4'))->comment = "1=Inquery ,2=confirm,3=outstanding,4=inactive";
            $table->string('firm_admin_username');
            $table->string('firm_admin_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lawfirm_mgmt', function($table)
        {
            $table->dropColumn(array('firm_no_of_users', 'firm_no_of_cases', 'firm_non_active_users','firm_unique_id','firm_status','firm_admin_username','firm_admin_password'));
        });
    }
}
