<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLawfirmMgmtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawfirm_mgmt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firm_name',255);
            $table->string('firm_address');
            $table->integer('firm_state');
            $table->integer('firm_country');
            $table->integer('firm_city');
            $table->string('firm_contact_number',20);
            $table->string('firm_fax',20);
            $table->string('firm_email');
            $table->string('firm_concern_person',30);
            $table->ipAddress('firm_ip');
            $table->string('firm_subdomain');
            $table->string('firm_logo');
            $table->string('firm_support_person',30);
            $table->string('firm_support_contact',20);
            $table->string('firm_support_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawfirm_mgmt');
    }
}
